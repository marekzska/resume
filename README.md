# Môj webový životopis

Single page web app s účelom doručenia najhodnotnejších informácií pre potenciálnych zamestnávateľov

## Stručný popis:



### Použité technológie: 

Aplikácia bola vytvorená pomocou create-react-app a je hostovaná na mojej subdoméne. Na vývoj som používal React a Javascript, štylizované komponenty, jednoduché animácie sú vytvorené pomocou @keyframes a mobilné zobrazenie závisí od šírky obrazovky zariadenia pomocou @media queries. Pozadie tvorí knižnica tsparticles.

### Obsah: 

Zachytáva moje zručnosti v oblasti web, tech a soft skills, ako aj moje doterajšie skúsenosti a vzdelanie. Obsahuje tiež kontaktné informácie v prípade, že by som Vás zaujal.