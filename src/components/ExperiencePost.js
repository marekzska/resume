import React from "react";
import texts from "../resources/texts";
import '../css/post.css'

function ExperiencePost(props) {
  return (
    <div id='experiencePost'>
      <div className='companyName'>{texts.experience[props.index].company}</div>
      <div className='details'>
        {texts.experience[props.index].position},{" "}
        {texts.experience[props.index].duration[props.langSwitch ? "eng" : "svk"]}
      </div>
      <br />
      <div className='technologies'>
        {texts.experience[props.index].technologies}
      </div>
      <br />
      <div className='sentence'>
        {props.langSwitch
          ? "My resposibilities consisted of, but were not limited to:"
          : "Moja práca zahŕňala (nie len):"}
      </div>
      <div className='responsibilities'>
        <ul>
          {texts.experience[props.index].responsibilities[
            props.langSwitch ? "eng" : "svk"
          ].map((item, index) => {
            return <li key={"listItem"+index}>{item}</li>;
          })}
        </ul>
      </div>
    </div>
  );
}

export default ExperiencePost;
