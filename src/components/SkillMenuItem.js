import React from 'react'

function SkillMenuItem(props) {
  return (
    <div
      onClick={props.onSkillClick}
      className={
        props.skillType === props.children ? "selected skillType" : "skillType"
      }
    >
      {props.children}
    </div>
  );
}

export default SkillMenuItem