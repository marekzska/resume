import React from "react";
import "../css/languageSwitch.css";

function LanguageSwitch(props) {
  return (
    <div id='langLine' className={props.className}>
      SVK&nbsp;
      <label className='switch'>
        <input
          type='checkbox'
          onClick={props.onLangSwitch}
          defaultChecked={props.langSwitch}
        />
        <span className='slider'></span>
      </label>
      &nbsp;ENG
    </div>
  );
}

export default LanguageSwitch;
