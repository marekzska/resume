import React from "react";
import Skill from "./Skill.js";
import Category from "./Category.js";
import texts from "../resources/texts.js";

function SkillType(props) {
  const numbers = [
    ["five", 5],
    ["four", 4],
    ["three", 3],
    ["two", 2],
    ["one", 1],
  ];
  const levels = {
    eng: ["Minimal", "Basic", "Intermediate", "Advanced", "Profficient"],
    svk: ["Minimálna", "Základná", "Priemerná", "Pokročilá", "Zdatná"],
  };
  const skillTypeName = props.skillType.toLowerCase().split(" ")[0];
  const lang = props.langSwitch ? "eng" : "svk";

  return (
    <div id='skillBoard'>
      {numbers.map((number) => {
        return (
          texts[skillTypeName]?.[number[0]] && (
            <Category
              title={levels[lang][number[1] - 1]}
              key={
                "skill" +
                number[0] +
                "" +
                skillTypeName
              }
            >
              {texts[skillTypeName]?.[number[0]][
                lang
              ].map((item, index) => {
                return (
                  <Skill
                    key={item + "" + index}
                    style={{
                      animation: "fadeInBottom ease-in-out 0.1s forwards",
                      animationDelay: index * 50 + "ms",
                    }}
                    level={number[1]}
                  >
                    {item}
                  </Skill>
                );
              })}
            </Category>
          )
        );
      })}
    </div>
  );
}

export default SkillType;
