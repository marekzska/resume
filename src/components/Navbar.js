import React from "react";
import { NavLink, Link } from "react-router-dom";
import "../css/navbar.css";
import LanguageSwitch from "./LanguageSwitch";

function Navbar({ langSwitch, onLangSwitch }) {
  return (
    <div id='pageHeader'>
      <Link to='/'>
        <div id='profilePicture' className='navItem neverActive'>
          <img src='/resume/icon.png' alt='my head' />
        </div>
      </Link>
      <div id='navigation'>
        <NavLink to='skills' className='navItem navLink'>
          {langSwitch === true ? "Skills" : "Schopnosti"}
        </NavLink>
        <NavLink to='/experience' className='navItem navLink'>
          {langSwitch === true ? "Experience" : "Skúsenosti"}
        </NavLink>
        <NavLink to='/education' className='navItem navLink'>
          {langSwitch === true ? "Education" : "Vzdelanie"}
        </NavLink>
        <NavLink to='/portfolio' className='navItem navLink'>
          {langSwitch === true ? "Portfolio" : "Portfólio"}
        </NavLink>
      </div>
      <LanguageSwitch
        langSwitch={langSwitch}
        onLangSwitch={onLangSwitch}
        className='navItem'
      />
    </div>
  );
}

export default Navbar;
