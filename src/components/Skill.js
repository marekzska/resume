import React, { useEffect, useState } from "react";
import "../css/skill.css";

function Skill(props) {
  const [color, setColor] = useState();
  useEffect(() => {
    if (props.level === 1) {
      setColor("minimal");
    } else if (props.level === 2) {
      setColor("basic");
    } else if (props.level === 3) {
      setColor("intermediate");
    } else if (props.level === 4) {
      setColor("advanced");
    } else if (props.level === 5) {
      setColor("profficient");
    }
  }, [props.level]);
  return (
    <div style={props.style} className={"skillWrap " + color + " " + props.className}>
      {props.children}
    </div>
  );
}

export default Skill;
