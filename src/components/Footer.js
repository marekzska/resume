import React from "react";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import PhoneAndroidIcon from "@mui/icons-material/PhoneAndroid";
import EmailIcon from "@mui/icons-material/Email";
import "../css/footer.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSquareGitlab } from "@fortawesome/free-brands-svg-icons";
import { faDownload } from "@fortawesome/free-solid-svg-icons";

function Footer({langSwitch}) {
  return (
    <div id='footerInfo'>
      <a
        href='tel:+420608675592'
        id='footerPhone'
        target='_blank'
        rel='noreferrer'
      >
        <PhoneAndroidIcon />
        &nbsp; Phone
      </a>
      <a
        href='mailto:marekzska@gmail.com'
        id='footerEmail'
        target='_blank'
        rel='noreferrer'
      >
        <EmailIcon />
        &nbsp; E-Mail
      </a>
      <a
        href='https://www.linkedin.com/in/marekzska'
        id='footerLinkedIn'
        target='_blank'
        rel='noreferrer'
      >
        <LinkedInIcon />
        &nbsp; LinkedIn
      </a>
      <a
        href='https://gitlab.com/marekzska'
        id='footerGitlab'
        target='_blank'
        rel='noreferrer'
      >
        <FontAwesomeIcon icon={faSquareGitlab} style={{ fontSize: "2vh" }} />{" "}
        &nbsp;&nbsp;&nbsp; Gitlab
      </a>
      <a href='./marek_ziska_CV_english.pdf' id='downloadFooterCV' download>
        <FontAwesomeIcon icon={faDownload} style={{ fontSize: "2vh" }} />{" "}
        &nbsp;&nbsp;&nbsp;
        {langSwitch ? "Download my CV" : "Stiahnite si moje CV"}
      </a>
    </div>
  );
}

export default Footer;
