import React from 'react'
import '../css/category.css'

function Category(props) {
  return (
    <div className='category'>
      <div className='cilinder'>
        {props.children}
      </div>
      <div className='cilinderTitle'>{props.title}</div>
    </div>
  );
}

export default Category