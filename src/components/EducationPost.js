import React from "react";
import texts from "../resources/texts";
import "../css/post.css";

function EducationPost(props) {
  return (
    <div id='educationPost'>
      <div className='schoolName'>{texts.education[props.index].school}</div>
      <div className='details'>
        {
          texts.education[props.index].programme[
            props.langSwitch ? "eng" : "svk"
          ]
        }
        ,{" "}
        {
          texts.education[props.index].duration
        }
      </div>
      <br />
      <div className='disclaimer'>
        {
          texts.education[props.index].disclaimer[
            props.langSwitch ? "eng" : "svk"
          ]
        }
      </div>
      <br />
      <div className='sentence'>
        {props.langSwitch
          ? "I learned (not only):"
          : "Naučil som sa (nie len):"}
      </div>
      <div className='curriculum'>
        <ul>
          {texts.education[props.index].curriculum[
            props.langSwitch ? "eng" : "svk"
          ].map((item, index) => {
            return <li key={"listItem"+index}>{item}</li>;
          })}
        </ul>
      </div>
    </div>
  );
}

export default EducationPost;
