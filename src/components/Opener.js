import React from "react";
import "../css/opener.css";

function Opener() {
  return (
    <>
      <div
        id='topTriangle'
        style={{
          borderWidth: `${document.body.offsetHeight}px ${document.body.offsetWidth}px 0 0`,
        }}
      ></div>
      <div
        id='bottomTriangle'
        style={{
          borderWidth: `0 0 ${document.body.offsetHeight}px ${document.body.offsetWidth}px`,
        }}
      ></div>
      <div id='logo'>
        <div id='profile'>
          <img src='./icon.png' alt='A drawing of me' />
        </div>
        <div id='name'>
          {"MAREKŽIŠKA".split("").map((item, index) => {
            if (index === 5) {
              return (
                <React.Fragment key='fragment'>
                  <br key={index + "break"} />
                  <div className='letter' key={"letterBelowFive" + index}>
                    {item}
                  </div>
                </React.Fragment>
              );
            } else {
              return (
                <div className='letter' key={"letterOverFive" + index}>
                  {item}
                </div>
              );
            }
          })}
        </div>
      </div>
    </>
  );
}

export default Opener;
