import "./App.css";
import ParticlesBackground from "./components/ParticlesBackground";
import React, { useEffect, useState } from "react";
import Opener from "./components/Opener";
import Navbar from "./components/Navbar";
import { Routes, Route, useLocation } from "react-router-dom";
import Skills from "./pages/Skills";
import Intro from "./pages/Intro.js";
import Experience from "./pages/Experience";
import Education from "./pages/Education";
import MobileView from "./pages/MobileView";
import Footer from "./components/Footer";
import Portfolio from "./pages/Portfolio";

function App() {
  const [visible, setVisible] = useState(true);
  const [langSwitch, setLangSwitch] = useState(true);
  const location = useLocation();
  useEffect(() => {
    let clear = setTimeout(() => {
      setVisible(false);
    }, 6500);

    return () => clearTimeout(clear);
  }, []);

  const handleLangSwitch = () => setLangSwitch(!langSwitch);

  if (window.screen.width <= 600) {
    return (
      <>
        {visible ? (
          <Opener />
        ) : (
          <MobileView langSwitch={langSwitch} onLangSwitch={handleLangSwitch} />
        )}
      </>
    );
  } else {
    return (
      <>
        {visible ? (
          <Opener />
        ) : (
          <>
            <Navbar langSwitch={langSwitch} onLangSwitch={handleLangSwitch} />
            <ParticlesBackground />
            <Routes location={location} key={location.pathname}>
              <Route path='/' element={<Intro langSwitch={langSwitch} />} />
              <Route
                path='/skills'
                element={<Skills langSwitch={langSwitch} />}
              />
              <Route
                path='/experience'
                element={<Experience langSwitch={langSwitch} />}
              />
              <Route
                path='/education'
                element={<Education langSwitch={langSwitch} />}
              />
              <Route
                path='/portfolio'
                element={<Portfolio langSwitch={langSwitch} />}
              />
            </Routes>
            <Footer langSwitch={langSwitch} />
          </>
        )}
      </>
    );
  }
}

export default App;
