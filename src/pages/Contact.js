import React from "react";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import PhoneAndroidIcon from "@mui/icons-material/PhoneAndroid";
import EmailIcon from "@mui/icons-material/Email";
import "../css/contact.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSquareGitlab } from "@fortawesome/free-brands-svg-icons";

function Contact(props) {
  return (
    <div id='contactWrap'>
      <div id='contactName'>Marek Žiška</div>
      <div id='contactInfo'>
        <a
          href='tel:+420608675592'
          id='contactPhone'
          target='_blank'
          rel='noreferrer'
        >
          <PhoneAndroidIcon />
          &nbsp; Phone
        </a>
        <a
          href='mailto:marekzska@gmail.com'
          id='contactEmail'
          target='_blank'
          rel='noreferrer'
        >
          <EmailIcon />
          &nbsp; E-Mail
        </a>
        <a
          href='https://www.linkedin.com/in/marekzska'
          id='contactLinkedIn'
          target='_blank'
          rel='noreferrer'
        >
          <LinkedInIcon />
          &nbsp; LinkedIn
        </a>
        <a
          href='https://gitlab.com/marekzska'
          id='contactGitlab'
          target='_blank'
          rel='noreferrer'
        >
          <FontAwesomeIcon icon={faSquareGitlab} style={{ fontSize: "2vh" }} />{" "}
          &nbsp;&nbsp;&nbsp; Gitlab
        </a>
        <a href='./marek_ziska_CV_english.pdf' id='downloadCV' download>
          {props.langSwitch ? "Download my CV" : "Stiahnite si moje CV"}
        </a>
      </div>
    </div>
  );
}

export default Contact;
