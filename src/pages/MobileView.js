import React from "react";
import Navbar from "../components/Navbar";
import ParticlesBackground from "../components/ParticlesBackground";
import Intro from "./Intro";
import '../css/mobileView.css'
import Skills from "./Skills";
import Experience from "./Experience";
import Education from "./Education";
import Contact from "./Contact";


function MobileView(props) {
  return (
    <div id='mobileView'>
      <Navbar langSwitch={props.langSwitch} onLangSwitch={props.onLangSwitch} />
      <ParticlesBackground />
      <Intro langSwitch={props.langSwitch} />
      <Skills langSwitch={props.langSwitch} />
      <Experience langSwitch={props.langSwitch} />
      <Education langSwitch={props.langSwitch} />
      <Contact langSwitch={props.langSwitch} />
    </div>
  );
}

export default MobileView;
