import React, { useState } from "react";
import ExperiencePost from "../components/ExperiencePost";
import "../css/experienceAndEducation.css";
import texts from "../resources/texts";

function Experience(props) {
  const [index, setIndex] = useState(0);

  function handleArrowClick(direction) {
    if (direction === "+") {
      if (index === texts.experience.length - 1) {
        return () => setIndex(0);
      } else {
        return () => setIndex(index + 1);
      }
    } else if (direction === "-") {
      if (index === 0) {
        return () => setIndex(texts.experience.length - 1);
      } else {
        return () => setIndex(index - 1);
      }
    }
  }

  return (
    <div id='experienceWrap'>
      <div id='experienceText'>
        <ExperiencePost langSwitch={props.langSwitch} index={index} />
      </div>
      <div className='postNav'>
        <div
          className='arrow'
          id='arrowLeft'
          onClick={handleArrowClick("-")}
        ></div>
        <div>
          {index + 1}
          {"/"}
          {texts.experience.length}
        </div>
        <div
          className='arrow'
          id='arrowRight'
          onClick={handleArrowClick("+")}
        ></div>
      </div>
    </div>
  );
}

export default Experience;
