import React, { useState } from "react";
import "../css/skills.css";
import SkillType from "../components/SkillType";
import SkillMenuItem from "../components/SkillMenuItem";

function Skills({langSwitch}) {
  const [skillType, setSkillType] = useState("Web skills");

  const handleSkillClick = (e) => {
    setSkillType(e.target.textContent);
  };
  return (
    <div id='skillsShowcase'>
      
        <SkillType skillType={skillType} langSwitch={langSwitch} />

      <div id='skillBar'>
        <div id='skillMenu'>
          <SkillMenuItem skillType={skillType} onSkillClick={handleSkillClick}>Web skills</SkillMenuItem>
          <SkillMenuItem skillType={skillType} onSkillClick={handleSkillClick}>Tech skills</SkillMenuItem>
          <SkillMenuItem skillType={skillType} onSkillClick={handleSkillClick}>Soft skills</SkillMenuItem>
        </div>
      </div>
    </div>
  );
}

export default Skills;
