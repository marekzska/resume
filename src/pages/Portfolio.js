import { text } from "@fortawesome/fontawesome-svg-core";
import React from "react";
import "../css/portfolio.css";
import texts from "../resources/texts";

function Portfolio({ langSwitch }) {
  return (
    <div className='portfolioWrap'>
      <div className='exhibit'>
        <iframe title='Snake Game' src={texts.portfolio.serpent.link} />
        <span className='exhibitDescription'>
          {texts.portfolio.serpent.desc[langSwitch ? "eng" : "svk"]}
          <br />
          <br />
          <a href={texts.portfolio.serpent.link} target='_blank'>
            Here
          </a>
        </span>
      </div>
      <div className='exhibit'>
        <iframe
          title='Hand Snake Game'
          src={texts.portfolio.handSerpent.link}
        />
        <span className='exhibitDescription'>
          {texts.portfolio.handSerpent.desc[langSwitch ? "eng" : "svk"]}
          <br />
          <br />
          <a href={texts.portfolio.handSerpent.link} target='_blank'>
            Here
          </a>
        </span>
      </div>
      <div className='exhibit'>
        <iframe title='World Map' src={texts.portfolio.worldMap.link} />
        <span className='exhibitDescription'>
          {texts.portfolio.worldMap.desc[langSwitch ? "eng" : "svk"]}
          <br />
          <br />
          <a href={texts.portfolio.worldMap.link} target='_blank'>
            Here
          </a>
        </span>
      </div>
    </div>
  );
}

export default Portfolio;
