import React, { useState } from "react";
import EducationPost from "../components/EducationPost";
import "../css/experienceAndEducation.css";
import texts from "../resources/texts";

function Education(props) {
  const [index, setIndex] = useState(0);

  function handleArrowClick(direction) {
    if (direction === "+") {
      if (index === texts.education.length - 1) {
        return () => setIndex(0);
      } else {
        return () => setIndex(index + 1);
      }
    } else if (direction === "-") {
      if (index === 0) {
        return () => setIndex(texts.education.length - 1);
      } else {
        return () => setIndex(index - 1);
      }
    }
  }

  return (
    <div id='educationWrap'>
      <div id='educationText'>
        <EducationPost langSwitch={props.langSwitch} index={index} />
      </div>
      <div className='postNav'>
        <div
          className='arrow'
          id='arrowLeft'
          onClick={handleArrowClick("-")}
        ></div>
        <div>
          {index + 1}
          {"/"}
          {texts.experience.length}
        </div>
        <div
          className='arrow'
          id='arrowRight'
          onClick={handleArrowClick("+")}
        ></div>
      </div>
    </div>
  );
}

export default Education;
