import React, { useState, useEffect, useMemo } from "react";
import "../css/intro.css";
import texts from "../resources/texts.js";
import SwipeDownIcon from "@mui/icons-material/SwipeDown";

function Intro(props) {
  const [phrase, setPhrase] = useState();
  const phrases = useMemo(
    () => [
      { eng: ["page", "portfolio", "resumé", "skill", "will to learn"] },
      {
        svk: [
          "je moja stránka",
          "je moje portfólio",
          "je môj životopis",
          "sú moje schopnosti",
          "je moja chuť učiť sa",
        ],
      },
    ],
    []
  );
  useEffect(() => {
    if (props.langSwitch === true) {
      setPhrase(phrases[0].eng[0]);
    } else {
      setPhrase(phrases[1].svk[0]);
    }
  }, [phrases, props.langSwitch]);
  useEffect(() => {
    let count = 0;
    let clear = setInterval(() => {
      if (props.langSwitch === true) {
        if (count === phrases[0].eng.length) {
          count = 0;
        }
        setPhrase(phrases[0].eng[count]);
        count++;
      } else {
        if (count === phrases[1].svk.length) {
          count = 0;
        }
        setPhrase(phrases[1].svk[count]);
        count++;
      }
    }, 2500);
    return () => clearInterval(clear);
  }, [phrases, props.langSwitch]);

  return (
    <div id='intro'>
      <div id='textWrap'>
        <h1 id='introText'>
          {props.langSwitch === true ? "I am Marek" : "Som Marek"}
        </h1>
        <h3 id='introTextSecond'>
          {props.langSwitch === true
            ? `and this is my ${phrase}`
            : `a toto ${phrase}`}
        </h3>
      </div>
      <div id='introParagraph'>
        {texts.intro[props.langSwitch ? "eng" : "svk"].map((item, index) => {
          return (
            <React.Fragment key={"introFragment" + index}>
              {item}
              <br key={"break1index" + index} />
              <br key={"break2index" + index} />
            </React.Fragment>
          );
        })}
      </div>
      <SwipeDownIcon id="swipeDown" />
    </div>
  );
}

export default Intro;
