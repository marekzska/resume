const particles = {
  fullScreen: {
    enable: false,
    zIndex: -1,
  },
  interactivity: {
    events: {
      onClick: {
        enable: false,
        mode: "push",
      },
      onHover: {
        enable: true,
        mode: "repulse",
      },
    },
    modes: {
      push: {
        quantity: 10,
      },
      repulse: {
        distance: 100,
      },
    },
  },
  particles: {
    links: {
      enable: true,
      color: {
        value: "1e90ff",
      },
      distance: 150,
      opacity: 0.2,
    },
    color: {
      value: { min: "d60270", max: "0038a8" },
    },
    number: {
      value: "100",
    },
    move: {
      enable: true,
      speed: { min: 0.5, max: 1 },
    },
    opacity: {
      value: { min: 0.1, max: 0.2 },
    },
    size: {
      value: { min: 1, max: 3 },
    },
  },
};
export default particles;
