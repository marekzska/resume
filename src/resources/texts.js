const texts = {
  web: {
    five: {
      eng: [
        "HTML5",
        "CSS3",
        "JavaScript",
        "DOM Manipulation",
        "ECMAScript6",
        "JSON",
      ],
      svk: [
        "HTML5",
        "CSS3",
        "JavaScript",
        "DOM Manipulation",
        "ECMAScript6",
        "JSON",
      ],
    },
    four: {
      eng: [
        "Responsive Design",
        "React",
        "SPA",
        "Developer Tools",
        "AJAX",
        "PHP",
      ],
      svk: [
        "Responsive Design",
        "React",
        "SPA",
        "Developer Tools",
        "AJAX",
        "PHP",
      ],
    },
    three: {
      eng: ["SQL", "REST API", "GIT", "Fetch", "React Router", "Bootstrap"],
      svk: ["SQL", "REST API", "GIT", "Fetch", "React Router", "Bootstrap"],
    },
    two: {
      eng: ["Material UI", "Formik"],
      svk: ["Material UI", "Formik"],
    },
    one: {
      eng: ["Axios", "PWA", "LESS", "SASS", "TypeScript", "Vue.js"],
      svk: ["Axios", "PWA", "LESS", "SASS", "TypeScript", "Vue.js"],
    },
  },
  tech: {
    four: {
      eng: ["Twilio", "C", "Microsoft Office"],
      svk: ["Twilio", "C", "Microsoft Office"],
    },
    three: {
      eng: [
        "Algorithms",
        "C++",
        "Python",
        "Python Kivy",
        "Linux Bash",
        "Postman",
        "Adobe Photoshop",
        "Adobe InDesign",
        "Figma",
      ],
      svk: [
        "Algorithms",
        "C++",
        "Python",
        "Python Kivy",
        "Linux Bash",
        "Postman",
        "Adobe Photoshop",
        "Adobe InDesign",
        "Figma",
      ],
    },
    two: {
      eng: ["Adobe Illustrator"],
      svk: ["Adobe Illustrator"],
    },
  },
  soft: {
    five: {
      eng: ["Problem Solving", "Written Communication", "Organized"],
      svk: ["riešenie problémov", "Písomná komunikácia", "Organizovanosť"],
    },
    four: {
      eng: [
        "Verbal Communication",
        "Attention to detail",
        "Punctuality",
        "Time Management",
        "Critical Thinking",
      ],
      svk: [
        "Verbálna komunikácia",
        "Dôraz na detail",
        "Dochvíľnosť",
        "Time Management",
        "Kritické myslenie",
      ],
    },
    three: {
      eng: ["Creativity", "Teamwork", "Adaptability"],
      svk: ["Kreativita", "Spolupráca", "Adaptabilita"],
    },
    two: { eng: ["Leadership"], svk: ["Leadership"] },
  },
  intro: {
    eng: [
      "I am an aspiring developer with 3+ years of professional experience in all fundamental web-development languages (HTML, CSS, JS, PHP). I have also completed a course on React library, from which I have obtained a certificate, and have been honing my skill for almost a year now.",
      "I am currently looking for a position of Junior Web Developer with focus on JavaScript/React and I am hoping for an open and patient collective of colleagues, since my skills are still under development.",
      "I will appreciate any guidance in the field, whilst aiming for the maximum amount of self-reliance. I consider myself a quick, determined learner and I hope to conquer new technologies soon.",
    ],
    svk: [
      "Som ašpirujúcim vývojárom s 3+ rokmi profesionálnej praxe, využívajúc všetky základné jazyky určené na vývoj webstránok. (HTML, CSS, JS, PHP). Taktiež som úspešne dokončil kurz knižnice React, z ktorého som dostal certifikát, a zlepšujem sa v jeho používaní už takmer rok.",
      " V súčasnosti by som sa rád zamestnal ako Junior Web Developer s dôrazom na JavaScript/React a dúfam v otvorený a trpezlivý kolektív kolegov, kedže moje schopnosti sú stále vo vývoji.",
      " Ocením akékoľvek vedenie, dbajúc na maximálne možné množstvo samostatnosti. Považujem sa za rýchleho a odhodlaného študenta a v blízkej budúcnosti plánujem pokoriť nové technológie.",
    ],
  },
  experience: [
    {
      company: "Ipsos s.r.o.",
      position: "IT Project Manager",
      duration: { eng: "Feb 2020 - Present", svk: "Feb 2020 - Prítomnosť" },
      technologies:
        "HTML, CSS, JavaScript, jQuery, PHP, Python, Twilio, PHPMailer, API, MicroSIP, Nfield Odin, JSON, VoIP Blazer, PWA",
      responsibilities: {
        eng: [
          "Reduction of development costs by designing, creating and integrating solutions myself, instead of habitual outsourcing to external agencies",
          "Creating intuitive tools for recurring requests, reducing number of people in the workflow",
          "Overall increase in effectivity and reduction of time consumed by tasks since my arrival",
          "Preparation of JavaScript templates for surveys",
          "Custom solutions for unique projects (e.g. Creating a PWA for logging attendance based on Google Authenticator for Škoda CZ)",
          "Database management",
          "Automated SMS and emails using Twilio API and PHPmailer",
          "HTML email templates",
          "Technical support for Czechia, Slovakia, Hungary and Austria",
        ],
        svk: [
          "Zníženie nákladov na vývoj dizajnovaním, tvorbou a integráciou riešení interne, namiesto zaužívaného outsourcovania externým agentúram",
          "Tvorba intuitívnych nástrojov pre opakujúce sa požiadavky, čím bolo redukované množstvo ľudí vo workflow",
          "Celkové zvýšenie efektivity a zníženie času venovaného úlohám",
          "Príprava JavaScriptových šablón pre dotazníky",
          "Riešenia na mieru pre ojedinelé projekty (napr. Vytvorenie PWA dochádzkového systému založeného na Google Authenticator pre Škoda CZ",
          "Správa databáz",
          "Automatizované odosielanie SMS a emailov pomocou Twilio API a PHPmailer",
          "HTML emailové šablóny",
          "Technická podpora pre Českú republiku, Slovensko, Maďarsko a Rakúsko",
        ],
      },
    },
    {
      company: "Aplik s.r.o.",
      position: "Intern",
      duration: { eng: "Jul 2019 - Sep 2019", svk: "Júl 2029 - Sep 2019" },
      technologies: "HTML, CSS, JavaScript, jQuery, PHP, AJAX, JSON,",
      responsibilities: {
        eng: [
          "Development of web application for monitoring water management facilities and data visualisation",
          "Design and development of full-stack web interface",
          "Remote control of water management objects",
          "Real-time data visualisation in a historic and real-time progress graph using sensor-collected data ",
        ],
        svk: [
          "Vývoj webovej aplikácie za účelom monitoringu vodohospodárskych objektov a vizualizácie nazbieraných dát",
          "Dizajn a vývoj full-stack webového rozhrania",
          "Diaľkové ovládanie vodohospodárkych objektov",
          "Vizualizácia dát v reálnom čase a v historickom grafe",
        ],
      },
    },
  ],
  education: [
    {
      school: "Fakulta informačních technologií ČVUT",
      programme: {
        eng: "Bachelor's Degree (Web Development)",
        svk: "Bakalárske štúdium (Webový Vývoj)",
      },
      disclaimer: {
        eng: "One of the leading technical universities in Europe",
        svk: "Jedna z vedúcich technických univerzít v Európe",
      },
      duration: "Sep 2019 - Jun 2022",
      curriculum: {
        eng: [
          "Linux Bash Scripting",
          "Basics of GIT usage",
          "C/C++ Programming",
          "Object Oriented Programming",
          "Relational Databases",
          "Graph Theory",
          "(Non)Deterministic Finite Automata",
          "Basic Algorithms (Recursion, Sorts, BFS/DFS, Binary Search, etc.)",
          "Basic Data Structures (queue, stack, linked list, map, vector, array, binary search tree, etc.)",
        ],
        svk: [
          "Linux Bash Scripting",
          "Zaklady používania GITu",
          "C/C++ Programovanie",
          "Objektovo Orientované Programovanie",
          "Relačné databázy",
          "Teória Grafov",
          "(Ne)Deterministické konečné automaty",
          "Základné algoritmy (Rekurzia, Radenie, BFS/DFS, Binárne vyhľadávanie, atď.)",
          "Základné dátové štruktúry (rada, stack, spájaný zoznam, mapa, vektro, pole, binárny vyhľadávací strom, atď.)",
        ],
      },
    },
    {
      school: "Gymnázium Jura Hronca",
      programme: { eng: "Biligual Programme", svk: "Bilingválny Program" },
      disclaimer: {
        eng: "Ranked among top 10 grammar schools in the last ten years",
        svk: "Radí sa medzi top 10 najlepších gymnázií v posledných desiatich rokoch",
      },
      duration: "Sep 2014 - Jun 2019",
      curriculum: {
        eng: [
          "English (C1)",
          "Basics of programming (Object Pascal, Python)",
          "Graduated from Mathematics, Slovak language, English language and Informatics with a perfect record",
        ],
        svk: [
          "Angličtina (C1)",
          "Základy programovania (Object Pascal, Python)",
          "Zmaturoval som z Matematiky, Slovenčiny, Angličtiny a Infromatiky na samé jednotky",
        ],
      },
    },
  ],
  portfolio: {
    serpent: {
      desc: {
        eng: "A classic sname game controlled by arrows, my first project created using React",
        svk: "Klasická hra had ovládaná šípkami, môj prvý projekt vytvorený pomocou Reactu",
      },
      link: "https://marekzska.itervitae.eu/Serpent/",
    },
    handSerpent: {
      desc: {
        eng: "A game identical to the left one, however, this one can be controlled by gestures (thumb up/down, index finger pointing left/right)",
        svk: "Hra identická s vedľajšou, ale dá sa ovládať gestami (palec nahor/nadol, ukazovák doľava/doprava)",
      },
      link: "https://marekzska.itervitae.eu/HandSerpent/",
    },
    worldMap: {
      desc: {
        eng: "Geographical game based on knowledge of flags, capital cities and their positions on map. The only thing remaning for me to solve is the zoom. (If you play, make sure to choose answers by confirming them with Enter when choosing from the dropdown)",
        svk: "Geografická hra založená na znalostiach vlajok, hlavných miest a ich polôh na mape. Už mi zostáva iba vyriesiť približovanie. (Ak budete hrať, potvrďte možnosti z dropdownu stlačením klávesu Enter)",
      },
      link: "https://marekzska.itervitae.eu/Mapa/",
    },
  },
};
export default texts;
